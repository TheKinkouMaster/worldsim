# WorldSim

Long long time ago I wanted to make a world simulation/game. This is last of many attempts that I work on in free time. Currently creates planet with random temperature, and simulates flow in time.

At start program generates sun and planet orbiting it. Because distances between objects are almost 1:1 as in real world Unity has problems rendering it. Even after multiplying by whole set of ratios it is still bad. 
Whole thing needs to be rewritten so distances are still in game logic as they are now, but rendering has to be in more... appropriate for Unity values (less that 10billion units, roughly speaking).