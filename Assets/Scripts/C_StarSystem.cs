﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class C_StarSystem : MonoBehaviour
{
    public C_Map m_map;

    public float m_timeJump;
    public double m_velocity;
    public C_Constants m_constantDatabase;

    public void M_IterateTime()
    {
        
    }
    public void Awake()
    {
        M_Init();
    }
    public void M_Init()
    {
        double distanceFromSun = m_constantDatabase.m_planetDistance.number;
        double unsquaredVelocity = (m_constantDatabase.m_gravitationalConstant.number * m_constantDatabase.m_starMass.number) / distanceFromSun;
        m_velocity = Mathf.Sqrt((float)unsquaredVelocity) * Mathf.Pow(10, m_constantDatabase.m_starMass.multiplier + m_constantDatabase.m_gravitationalConstant.multiplier);
        Debug.Log(m_velocity);
    }
}
