﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class C_Map : MonoBehaviour
{
    public C_Constants m_constants;
    public GameObject m_fieldPrefab;
    public GameObject m_mapAnchor;
    public GameObject m_starAnchor;
    List<List<C_Field>> m_mapFields;
    public int m_sizex;
    public int m_sizey;
    public float m_rotationAxis;
    public float m_currentMovingAngle;
    public float m_minTemperatureAbs;
    public float m_MaxTemperatureAbs;
    public float m_minTemperature;
    public float m_maxTemperature;
    public bool m_showAbsTemp;

    List<bool> m_coroutineStatus;


    private void Awake()
    {
        m_coroutineStatus = new List<bool>()
        {
            false, false
        };
        M_InitMap();
        M_PositionMap();
    }
    private void Update()
    {
        if(m_coroutineStatus[0] == false && m_coroutineStatus[1] == false)
        {
            M_DetectMinMaxTemp();
            M_PositionPlanet();
            StartCoroutine(M_Equalizer());
            StartCoroutine(M_ColorFields(m_showAbsTemp));
            m_currentMovingAngle += m_constants.m_planetAngularVelocity.M_CastToFloat();
            m_mapAnchor.transform.RotateAround(m_mapAnchor.transform.position, m_mapAnchor.transform.up, m_constants.m_planetAngularVelocityAroundSelf.M_CastToFloat());
        }
        
    }

    public void M_PositionPlanet()
    {
        float x, y;
        m_constants.M_Init();
        x = m_constants.m_planetDistance.M_CastToFloat() * Mathf.Cos(m_currentMovingAngle * Mathf.Deg2Rad) * m_constants.m_distanceRatio;
        y = m_constants.m_planetDistance.M_CastToFloat() * Mathf.Sin(m_currentMovingAngle * Mathf.Deg2Rad) * m_constants.m_distanceRatio;
        m_mapAnchor.transform.position = new Vector3(x, 0, y);
    }

    #region Init
    public void M_InitMap()
    {
        M_InitMapFields();
        m_mapFields.Add(M_InitMapRow(1, "Nothern Pole", false));
        m_mapFields.AddRange(M_InitRowAndCollumns(m_sizey, m_sizex, ".x Nothern Field"));
        m_mapFields.Add(M_InitMapRow(m_sizex, "x Equador", true));
        m_mapFields.AddRange(M_InitRowAndCollumns(m_sizey, m_sizex, ".x Southern Field"));
        m_mapFields.Add(M_InitMapRow(1, "Southern Pole", false));
    }
    private void M_InitMapFields()
    {
        M_DeInitMap();
        m_mapFields = new List<List<C_Field>>();
    }
    private void M_DeInitMap()
    {
        if(m_mapFields != null)
        {
            for(int i = 0; i < m_mapFields.Count; i++)
            {
                for(int j = 0; j < m_mapFields[i].Count; j++)
                {
                    Destroy(m_mapFields[i][j].gameObject);
                }
            }
        }
    }
    private C_Field M_InitMapField(string newname)
    {
        C_Field retval;
        retval = Instantiate(m_fieldPrefab).GetComponent<C_Field>();
        retval.M_Init();
        retval.gameObject.name = newname;
        return retval;
    }
    private List<C_Field> M_InitMapRow(int rowSize, string newname, bool usePrefix)
    {
        List<C_Field> retval = new List<C_Field>();
        string newname2;
        for (int i = 0; i < rowSize; i++)
        {
            newname2 = newname;
            if(usePrefix == true)
            {
                newname2 = newname.Replace("x", i.ToString());
            }
            retval.Add(M_InitMapField(newname2));
        }
        return retval;
    }
    private List<List<C_Field>> M_InitRowAndCollumns(int sizey, int sizex, string newname)
    {
        List<List<C_Field>> retval = new List<List<C_Field>>();
        for (int i = 0; i < sizey; i++)
        {
            m_mapFields.Add(M_InitMapRow(sizex, i.ToString() + newname, true));
        }
        return retval;
    }
    #endregion



    public void M_PositionMap()
    {
        //attaching objects to parent
        foreach (List<C_Field> obj1 in m_mapFields)
            foreach (C_Field obj2 in obj1)
                obj2.transform.SetParent(m_mapAnchor.transform);

        //calculating size of individual fields
        float horizontalAngleRatio = 360.0f / m_sizex;
        float verticalAngleRatio = 90.0f / (m_sizey + 2);

        float signVariable;
        float lowerRadius;
        float lowerCirc;
        float fieldheight;
        m_constants.M_Init();
        float radius = m_constants.m_planetSize.M_CastToFloat() * m_constants.m_distanceRatio;
        for (int y = 0; y < m_mapFields.Count; y++) 
        {
            for(int x = 0; x < m_mapFields[y].Count; x++)
            {
                if(m_mapFields[y].Count == 1)
                {
                    signVariable = Mathf.Sign(y - m_sizey);
                    fieldheight = (2 * Mathf.PI * radius) / ((4 * m_sizey) + 6);
                    m_mapFields[y][x].transform.localScale = new Vector3(fieldheight * 0.2f, 0.2f, fieldheight * 0.2f);
                    m_mapFields[y][x].transform.localPosition = new Vector3(0, -signVariable * radius, 0);
                    m_mapFields[y][x].transform.Rotate(new Vector3(signVariable * 90, 0, 0));
                    m_mapFields[y][x].GetComponent<C_Field>().m_temperature = 0;
                }
                else
                {
                    //putting all fields in one spot
                    m_mapFields[y][x].transform.localPosition = new Vector3(radius, 0, 0);
                    //rotating to create rows
                    m_mapFields[y][x].transform.RotateAround(new Vector3(0,0,0),Vector3.up,-x*horizontalAngleRatio);
                    //rotating even rows to create hex-like map
                    if (y % 2 == 0)
                    {
                        m_mapFields[y][x].transform.RotateAround(new Vector3(0, 0, 0), Vector3.up, horizontalAngleRatio * 1 / 2);
                    }
                    //rotating to face outward sphere
                    m_mapFields[y][x].transform.Rotate(new Vector3(0, 0, -90));
                    //rotating to create sphere
                    m_mapFields[y][x].transform.RotateAround(new Vector3(0, 0, 0), m_mapFields[y][x].transform.right, (y - m_sizey - 1) * verticalAngleRatio);

                    //resizing fields
                    lowerRadius = Mathf.Sqrt((radius * radius) - (m_mapFields[y][x].transform.position.y * m_mapFields[y][x].transform.position.y));
                    lowerCirc = lowerRadius * 2 * Mathf.PI;
                    fieldheight = (2 * Mathf.PI * radius) / (m_mapFields.Count * 2);
                    m_mapFields[y][x].transform.localScale = new Vector3((lowerCirc/m_sizex)*0.1f,0.1f, fieldheight * 0.1f);
                    m_mapFields[y][x].GetComponent<C_Field>().m_temperature = 0;
                }
            }
        }
        //rotating
        m_mapAnchor.transform.Rotate(new Vector3(m_rotationAxis, 0, 0));
        m_mapAnchor.transform.position = new Vector3(m_constants.m_planetDistance.M_CastToFloat(), 0, 0);
        //setting up star
        M_InitStar();
    }
    public void M_InitStar()
    {
        float radius = m_constants.m_starSize.M_CastToFloat() * m_constants.m_distanceRatio;
        m_starAnchor.transform.localScale = new Vector3(radius, radius, radius);
        M_FindNeighbours();
        M_RandomizeTemperature(-40, 40);
        M_ColorFields(m_showAbsTemp);
    }
    
    
    


    #region Creating Planet
    public void M_FindNeighbours()
    {
        //DO NOT LOOK AT THIS THIS IS SPAGHETTI - MESSY BUT WORKS
        for (int y = 0; y < m_mapFields.Count; y++)
        {
            for (int x = 0; x < m_mapFields[y].Count; x++)
            {
                m_mapFields[y][x].m_otherTiles = new List<C_Field>();
                if (y == 0)
                {
                    m_mapFields[y][0].m_otherTiles.AddRange(m_mapFields[y + 1]);
                }
                else if (y == m_mapFields.Count - 1)
                {
                    m_mapFields[y][0].m_otherTiles.AddRange(m_mapFields[y - 1]);
                }
                else if (y == 1)
                {
                    m_mapFields[y][x].m_NETile = m_mapFields[y - 1][0];
                    m_mapFields[y][x].m_NWTile = m_mapFields[y - 1][0];
                    m_mapFields[y][x].m_SWTile = m_mapFields[y + 1][x];
                    if (x == 0)
                    {
                        m_mapFields[y][x].m_ETile = m_mapFields[y][x + 1];
                        m_mapFields[y][x].m_WTile = m_mapFields[y][m_mapFields[y].Count - 1];
                        m_mapFields[y][x].m_SETile = m_mapFields[y + 1][x + 1];
                    }
                    else if (x == m_mapFields[y].Count - 1)
                    {
                        m_mapFields[y][x].m_ETile = m_mapFields[y][0];
                        m_mapFields[y][x].m_WTile = m_mapFields[y][x - 1];
                        m_mapFields[y][x].m_SETile = m_mapFields[y + 1][0];
                    }
                    else
                    {
                        m_mapFields[y][x].m_ETile = m_mapFields[y][x + 1];
                        m_mapFields[y][x].m_WTile = m_mapFields[y][x - 1];
                        m_mapFields[y][x].m_SETile = m_mapFields[y + 1][x + 1];
                    }
                }
                else if (y == m_mapFields.Count - 2)
                {
                    m_mapFields[y][x].m_SETile = m_mapFields[y + 1][0];
                    m_mapFields[y][x].m_SWTile = m_mapFields[y + 1][0];
                    m_mapFields[y][x].m_NWTile = m_mapFields[y - 1][x];
                    if (x == 0)
                    {
                        m_mapFields[y][x].m_ETile = m_mapFields[y][x + 1];
                        m_mapFields[y][x].m_WTile = m_mapFields[y][m_mapFields[y].Count - 1];
                        m_mapFields[y][x].m_NETile = m_mapFields[y - 1][x + 1];
                    }
                    else if (x == m_mapFields[y].Count - 1)
                    {
                        m_mapFields[y][x].m_ETile = m_mapFields[y][0];
                        m_mapFields[y][x].m_WTile = m_mapFields[y][x - 1];
                        m_mapFields[y][x].m_NETile = m_mapFields[y - 1][0];
                    }
                    else
                    {
                        m_mapFields[y][x].m_ETile = m_mapFields[y][x + 1];
                        m_mapFields[y][x].m_WTile = m_mapFields[y][x - 1];
                        m_mapFields[y][x].m_NETile = m_mapFields[y - 1][x + 1];
                    }
                }
                else if (y % 2 == 0)
                {
                    m_mapFields[y][x].m_NETile = m_mapFields[y - 1][x];
                    m_mapFields[y][x].m_SETile = m_mapFields[y + 1][x];
                    if (x == 0)
                    {
                        m_mapFields[y][x].m_ETile = m_mapFields[y][x + 1];
                        m_mapFields[y][x].m_WTile = m_mapFields[y][m_mapFields[y].Count - 1];
                        m_mapFields[y][x].m_NWTile = m_mapFields[y - 1][m_mapFields[y].Count - 1];
                        m_mapFields[y][x].m_SWTile = m_mapFields[y + 1][m_mapFields[y].Count - 1];
                    }
                    else if (x == m_mapFields[y].Count - 1)
                    {
                        m_mapFields[y][x].m_ETile = m_mapFields[y][0];
                        m_mapFields[y][x].m_WTile = m_mapFields[y][x - 1];
                        m_mapFields[y][x].m_NWTile = m_mapFields[y - 1][x - 1];
                        m_mapFields[y][x].m_SWTile = m_mapFields[y + 1][x - 1];
                    }
                    else
                    {
                        m_mapFields[y][x].m_ETile = m_mapFields[y][x + 1];
                        m_mapFields[y][x].m_WTile = m_mapFields[y][x - 1];
                        m_mapFields[y][x].m_NWTile = m_mapFields[y - 1][x - 1];
                        m_mapFields[y][x].m_SWTile = m_mapFields[y + 1][x - 1];
                    }
                }
                else if (y % 2 != 0)
                {
                    m_mapFields[y][x].m_NWTile = m_mapFields[y - 1][x];
                    m_mapFields[y][x].m_SWTile = m_mapFields[y + 1][x];
                    if (x == 0)
                    {
                        m_mapFields[y][x].m_ETile = m_mapFields[y][x + 1];
                        m_mapFields[y][x].m_WTile = m_mapFields[y][m_mapFields[y].Count - 1];
                        m_mapFields[y][x].m_NETile = m_mapFields[y - 1][x + 1];
                        m_mapFields[y][x].m_SETile = m_mapFields[y + 1][x + 1];
                    }
                    else if (x == m_mapFields[y].Count - 1)
                    {
                        m_mapFields[y][x].m_ETile = m_mapFields[y][0];
                        m_mapFields[y][x].m_WTile = m_mapFields[y][x - 1];
                        m_mapFields[y][x].m_NETile = m_mapFields[y - 1][0];
                        m_mapFields[y][x].m_SETile = m_mapFields[y + 1][0];
                    }
                    else
                    {
                        m_mapFields[y][x].m_ETile = m_mapFields[y][x + 1];
                        m_mapFields[y][x].m_WTile = m_mapFields[y][x - 1];
                        m_mapFields[y][x].m_NETile = m_mapFields[y - 1][x + 1];
                        m_mapFields[y][x].m_SETile = m_mapFields[y + 1][x + 1];
                    }
                }
            }
        }
    }
    #endregion

    #region Updating Planet
    public int m_calculationInFrame;
    public IEnumerator M_ColorFields(bool absoluteColor)
    {
        m_coroutineStatus[0] = true;
        int iterator = 0;
        float temperature;
        Color color;
        foreach(List<C_Field> rows in m_mapFields)
            foreach(C_Field field in rows)
            {
                temperature = field.m_temperature;
                if (absoluteColor == false)
                {
                    color = M_FindColor(temperature, m_minTemperature, m_maxTemperature);
                }
                else
                {
                    color = M_FindColor(temperature, m_minTemperatureAbs, m_MaxTemperatureAbs);
                }
                field.m_meshRenderer.material.color = color;
                if (iterator >= m_calculationInFrame)
                {
                    iterator = 0;
                    yield return null;
                }
                else
                    iterator++;
            }
        m_coroutineStatus[0] = false;
    }
    public void M_RandomizeTemperature(float min, float max)
    {
        foreach (List<C_Field> rows in m_mapFields)
            foreach (C_Field field in rows)
            {
                field.m_temperature = (Random.value * (max - min)) + min;
            }
    }
    public void M_DetectMinMaxTemp()
    {
        m_minTemperature = 0;
        m_maxTemperature = 0;
        foreach (List<C_Field> fields in m_mapFields)
            foreach (C_Field field in fields)
            {
                if (m_minTemperature > field.m_temperature) m_minTemperature = field.m_temperature;
                if (m_maxTemperature < field.m_temperature) m_maxTemperature = field.m_temperature;
            }
    }
    public Color M_FindColor(float temp, float min, float max)
    {
        Color retval = Color.black;
        float sector = (temp - min) / (max - min);
        if(sector <= 0.25)
        {
            retval.r = 0;
            retval.g = Mathf.Lerp(0,1,sector*4);
            retval.b = 1;
        }
        else if(sector <= 0.50)
        {
            retval.r = 0;
            retval.g = 1;
            retval.b = Mathf.Lerp(1, 0, (sector-0.25f) * 4);
        }
        else if (sector <= 0.75)
        {
            retval.r = Mathf.Lerp(0, 1, (sector - 0.50f) * 4);
            retval.g = 1;
            retval.b = 0;
        }
        else if (sector <= 1.00)
        {
            retval.r = 1;
            retval.g = Mathf.Lerp(1, 0, (sector - 0.75f) * 4);
            retval.b = 0;
        }
        return retval;
    }
    #endregion

    #region Calculating Temperature
    public float m_jump;
    public IEnumerator M_Equalizer()
    {
        m_coroutineStatus[1] = true;
        int iterator = 0;
        float jump = m_jump;
        foreach (List<C_Field> fields in m_mapFields)
            foreach (C_Field field in fields)
            {
                M_RadiateTemperature(field);
                M_CalculateAngles(field);
                if (field.m_ETile != null) M_EqualizeValue(field, field.m_ETile, jump);
                if (field.m_WTile != null) M_EqualizeValue(field, field.m_WTile, jump);
                if (field.m_SETile != null) M_EqualizeValue(field, field.m_SETile, jump);
                if (field.m_SWTile != null) M_EqualizeValue(field, field.m_SWTile, jump);
                if (field.m_NETile != null) M_EqualizeValue(field, field.m_NETile, jump);
                if (field.m_NWTile != null) M_EqualizeValue(field, field.m_NWTile, jump);
                if (iterator >= m_calculationInFrame)
                {
                    iterator = 0;
                    yield return null;
                }
                else
                    iterator++;
            }
        m_coroutineStatus[1] = false;
    }
    public void M_EqualizeValue(C_Field obj1, C_Field obj2,float jump)
    {
        if (obj1.m_temperature > obj2.m_temperature)
        {
            obj1.m_temperature -= jump;
            obj2.m_temperature += jump;
        }
        else
        {
            obj1.m_temperature += jump;
            obj2.m_temperature -= jump;
        }
        
    }
    public float M_RadiationRatio = 365;
    public float M_TemperatureIncrease = 3;
    public void M_RadiateTemperature(C_Field obj1)
    {
        obj1.m_temperature -= Mathf.Exp(obj1.m_temperature / M_RadiationRatio);
    }
    public void M_CalculateAngles(C_Field field)
    {
        Vector3 vect = field.transform.up.normalized;
        float angle = Vector3.Angle(vect, field.transform.position);
        angle -= 90;
        if(angle > 0)
        {
            field.m_temperature += M_TemperatureIncrease * Mathf.Sin(Mathf.Deg2Rad * angle);
        }
        field.m_angle = angle;
    }
    #endregion
}
