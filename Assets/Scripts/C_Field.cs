﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class C_Field : MonoBehaviour
{
    public MeshRenderer m_meshRenderer;
    public float m_temperature;
    public float m_angle;
    public C_Field m_ETile;
    public C_Field m_WTile;
    public C_Field m_NETile;
    public C_Field m_NWTile;
    public C_Field m_SETile;
    public C_Field m_SWTile;
    public List<C_Field> m_otherTiles;

    public C_Field()
    {
    }
    public void M_Init()
    {
        m_meshRenderer = gameObject.GetComponent<MeshRenderer>();
        m_temperature = 0;
        m_ETile = null;
        m_WTile = null;
        m_NETile = null;
        m_NWTile = null;
        m_SETile = null;
        m_SWTile = null;
        m_otherTiles = new List<C_Field>();
    }
}
