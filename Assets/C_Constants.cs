﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class C_Constants : MonoBehaviour
{
    public SuperBigNumber m_planetSize;
    public SuperBigNumber m_planetDistance;
    public SuperBigNumber m_starSize;
    public SuperBigNumber m_starMass;
    public SuperBigNumber m_gravitationalConstant;
    public SuperBigNumber m_planetVelocity;
    public SuperBigNumber m_planetAngularVelocity;
    public SuperBigNumber m_planetAngularVelocityAroundSelf;
    public float m_distanceRatio;

    public double m_PS, m_PD, m_SZ, m_SM, m_GC, m_pav;
    public int m_PSm, m_PDm, m_SZm, m_SMm, m_GCm, m_pavm;

    public void Awake()
    {
        M_Init();
    }
    public void M_Init()
    {
        m_planetSize = new SuperBigNumber();
        m_planetDistance = new SuperBigNumber();
        m_starSize = new SuperBigNumber();
        m_starMass = new SuperBigNumber();
        m_gravitationalConstant = new SuperBigNumber();
        m_planetVelocity = new SuperBigNumber();
        m_planetAngularVelocity = new SuperBigNumber();
        m_planetSize.number = m_PS;
        m_planetSize.multiplier = m_PSm;
        m_planetDistance.number = m_PD;
        m_planetDistance.multiplier = m_PDm;
        m_starSize.number = m_SZ;
        m_starSize.multiplier = m_SZm;
        m_starMass.number = m_SM;
        m_starMass.multiplier = m_SMm;
        m_gravitationalConstant.number = m_GC;
        m_gravitationalConstant.multiplier = m_GCm;
        m_planetVelocity = SuperBigNumber.M_Multiply(m_gravitationalConstant, m_starMass);
        m_planetVelocity = SuperBigNumber.M_Devide(m_planetVelocity, m_planetDistance);
        m_planetVelocity = SuperBigNumber.M_SquareRoot(m_planetVelocity);
        m_planetAngularVelocity = SuperBigNumber.M_Devide(m_planetVelocity, m_planetDistance);
        m_planetAngularVelocity.number = (float)m_planetAngularVelocity.number * Mathf.Rad2Deg;
        m_planetAngularVelocityAroundSelf = new SuperBigNumber();
        m_planetAngularVelocityAroundSelf.number = m_pav;
        m_planetAngularVelocityAroundSelf.multiplier = m_pavm;
    }
    public class SuperBigNumber
    {
        public double number;
        public float multiplier;

        public float M_CastToFloat()
        {
            float retval = (float)number;
            retval = retval * Mathf.Pow(10, multiplier);
            return retval;
        }
        public double M_CastToDouble()
        {
            double retval = number;
            retval = Mathf.Pow((float)retval, multiplier);
            return retval;
        }
        static public SuperBigNumber M_Multiply(SuperBigNumber n1, SuperBigNumber n2)
        {
            SuperBigNumber retval = new SuperBigNumber
            {
                number = n1.number * n2.number,
                multiplier = n1.multiplier + n2.multiplier
            };
            return retval;
        }
        static public SuperBigNumber M_Devide(SuperBigNumber n1, SuperBigNumber n2)
        {
            SuperBigNumber retval = new SuperBigNumber
            {
                number = n1.number / n2.number,
                multiplier = n1.multiplier - n2.multiplier
            };
            return retval;
        }
        static public SuperBigNumber M_SquareRoot(SuperBigNumber n1)
        {
            SuperBigNumber retval = new SuperBigNumber
            {
                number = Mathf.Sqrt((float)n1.number),
                multiplier = n1.multiplier / 2
            };
            return retval;
        }
    }
}
