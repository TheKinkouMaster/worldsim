﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class C_CameraScript : MonoBehaviour
{
    public GameObject m_target;
    public C_Constants m_constants;

    public float distance;
    public float phi, ksi;
    public float sensitivity;
    private void Awake()
    {
        distance = m_constants.m_planetSize.M_CastToFloat() * m_constants.m_distanceRatio * 2;
    }
    private void Update()
    {
        transform.SetParent(m_target.transform);
        transform.localPosition = new Vector3(distance, 0, 0);
        transform.LookAt(transform.parent);
        transform.RotateAround(transform.parent.position,transform.parent.up, phi);
        transform.RotateAround(transform.parent.position, transform.right, ksi);
        M_ReadKeys();
    }
    public void M_ReadKeys()
    {
        if (Input.GetKey(KeyCode.LeftArrow)) phi = phi - sensitivity;
        if (Input.GetKey(KeyCode.RightArrow)) phi = phi + sensitivity;
        if (Input.GetKey(KeyCode.UpArrow)) ksi = ksi - sensitivity;
        if (Input.GetKey(KeyCode.DownArrow)) ksi = ksi + sensitivity;
    }
}
